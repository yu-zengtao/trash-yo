#pragma once
/**
* 定义硬件相关
*/
#define DHTTYPE DHT11 // 定义传感器类似 DHT11
#define DHTPIN 15 //宏定义DHT数据接口
//led灯使用了io4，溢出监测中断使用了io14
/**
* 定义智能配网
*/
#define HOST_NAME  "bemfa"
#define MAGIC_NUMBER 0xAA
#define CAMERA_MODEL_AI_THINKER  // Has PSRAM
/**
* 巴法云平台数据上传相关
*/
//----------------------------------------------------
//巴法云服务器
#define TCP_SERVER_ADDR "bemfa.com"
//服务器端口//TCP创客云端口8344//TCP设备云端口8340
#define TCP_SERVER_PORT "8344"

//设置上传速率2s（1s<=upDataTime<=60s）
#define upDataTime 2*1000
//最大字节数
#define MAX_PACKETSIZE 512