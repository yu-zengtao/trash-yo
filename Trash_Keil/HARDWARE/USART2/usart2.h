#ifndef __USART2_H
#define __USART2_H
#include "stdio.h"	
#include "sys.h" 

extern u8 res;
extern u8 rx_pointer;
extern char rxdata[30];
 
void uart2_Init(u32 My_BaudRate);
void uart_rx_proc(void);
	
#endif
