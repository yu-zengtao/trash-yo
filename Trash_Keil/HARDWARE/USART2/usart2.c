#include "usart2.h"
#include "delay.h"
#include "string.h"
#include "lcd.h"

u8 res;
u8 rx_pointer = 0;
char rxdata[30];
 
void uart2_Init(u32 My_BaudRate)
{
	GPIO_InitTypeDef GPIO_InitStrue;
	USART_InitTypeDef USART_InitStrue;
	NVIC_InitTypeDef NVIC_InitStrue;
	
	// 外设使能时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
	USART_DeInit(USART2);  //复位串口2 -> 可以没有
	
	// 初始化 串口对应IO口  TX-PA2  RX-PA3
	GPIO_InitStrue.GPIO_Mode=GPIO_Mode_AF_PP;
	GPIO_InitStrue.GPIO_Pin=GPIO_Pin_2;
	GPIO_InitStrue.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStrue);
	
	GPIO_InitStrue.GPIO_Mode=GPIO_Mode_IN_FLOATING;
	GPIO_InitStrue.GPIO_Pin=GPIO_Pin_3;
  GPIO_Init(GPIOA,&GPIO_InitStrue);
	
	// 初始化 串口模式状态
	USART_InitStrue.USART_BaudRate=My_BaudRate; // 波特率
	USART_InitStrue.USART_HardwareFlowControl=USART_HardwareFlowControl_None; // 硬件流控制
	USART_InitStrue.USART_Mode=USART_Mode_Tx|USART_Mode_Rx; // 发送 接收 模式都使用
	USART_InitStrue.USART_Parity=USART_Parity_No; // 没有奇偶校验
	USART_InitStrue.USART_StopBits=USART_StopBits_1; // 一位停止位
	USART_InitStrue.USART_WordLength=USART_WordLength_8b; // 每次发送数据宽度为8位
	USART_Init(USART2,&USART_InitStrue);
	
	USART_Cmd(USART2,ENABLE);//使能串口
	USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);//开启接收中断
	
	// 初始化 中断优先级
	NVIC_InitStrue.NVIC_IRQChannel=USART2_IRQn;
	NVIC_InitStrue.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStrue.NVIC_IRQChannelPreemptionPriority=2;
	NVIC_InitStrue.NVIC_IRQChannelSubPriority=3;
	NVIC_Init(&NVIC_InitStrue);
}
 
void uart_rx_proc(void)
{
	char false_value[10];  
  float first_number, second_number;  
	char text[30];
	if(rx_pointer > 0)
		printf("%s",rxdata);
	if(rxdata[0] == '.')
		LCD_ShowString(5,40,220,24,24,(u8 *)"waiting for connect"); 
	if(rxdata[1] == '*')
		LCD_ShowString(10,40,210,24,24,(u8 *)"smart config"); 
	if(rxdata[1] == 'D')
	{
		sscanf(rxdata, " DATA#%[^#]#%f#%f", false_value, &first_number, &second_number);
//		printf("false_value: %s\n", false_value);  
//    printf("first_number: %.2f\n", first_number);  
//    printf("second_number: %.2f\n", second_number); 
		sprintf(text,"%s,%0.2f,%0.2f",false_value,first_number,second_number);
		LCD_ShowString(5,70,210,24,24,(u8 *)text); 
	}
	rx_pointer = 0;
	memset(rxdata,0,30);
}

void USART2_IRQHandler(void) // 串口2中断服务函数
{
	if(USART_GetITStatus(USART2,USART_IT_RXNE)) // 中断标志
	{
		rxdata[rx_pointer++] = res;
		res= USART_ReceiveData(USART2);  // 串口2 接收
	}
}
