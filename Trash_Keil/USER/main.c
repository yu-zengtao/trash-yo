#include "sys.h"
#include "delay.h"							//延时驱动文件
#include "usart.h"
#include "usart2.h"
#include "led.h"
#include "lcd.h"
#include "exti.h"
#include "timer.h"
#include "paj7620u2.h"
#include "ms53l0m.h"

/* 主函数 */
int main(void)
{	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); //设置NVIC中断分组2:2位抢占优先级，2位响应优先级
	delay_init();							//延时初始化
	LED_Init();
	uart_init(115200);	    //串口初始化为115200
	uart2_Init(115200);
	EXTIX_Init();
	ESP32_Init();
	LCD_Init();
	LCD_Clear(WHITE);
	POINT_COLOR=RED;
	TIM1_PWM_Init();	//舵机初始化
	Ms53l0m_Init();                           //MS53L0M模块初始化
	LCD_ShowString(30,40,210,24,24,(u8 *)"owo"); 
	while(!paj7620u2_init())//PAJ7620U2传感器初始化
	{
		printf("PAJ7620U2 Error!!!\r\n");
		delay_ms(500);	
	}
	while(1)
	{  
			Gesture_test();
	}
}
